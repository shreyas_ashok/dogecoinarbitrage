# -*- coding: utf-8 -*-
import urllib,urllib2
from datetime import datetime
import time
from decimal import *
import hmac
import hashlib
import json

PUBLIC_KEY = "< insert public key here >"  
PRIVATE_KEY = "< insert private key here >"
BASE_API_URL = "https://www.coins-e.com/api/v2"


print "Coins-E Trade API access test" 
print "Get your API keys from https://www.coins-e.com/exchange/api/access/" 



def unauthenticated_request(url_suffix):
    url_request_object = urllib2.Request("%s/%s" % (BASE_API_URL,url_suffix))
    response = urllib2.urlopen(url_request_object)  
    print type(response)
    response_json = {}
    try:
        response_content = response.read()
        response_json = json.loads(response_content)        
        return response_json
    finally:
        response.close()
    return "failed"
    


def authenticated_request(url_suffix, method, post_args={}):
    nonce = 1000    
    try:
        f = open('coins-e_nonce', 'r')
        nonce = int(f.readline())
        f.close()
    finally:        
        f = open('coins-e_nonce', 'w')
        nonce += 1
        f.write(str(nonce))
        f.close()
    
    post_args['method'] = method        
    post_args['nonce'] = nonce        
    post_data = urllib.urlencode(post_args)
    required_sign = hmac.new(PRIVATE_KEY, post_data, hashlib.sha512).hexdigest()
    headers = {}
    headers['key'] = PUBLIC_KEY
    headers['sign'] = required_sign
    url_request_object = urllib2.Request("%s/%s" % (BASE_API_URL,url_suffix),
                                         post_data,
                                         headers)    
    response = urllib2.urlopen(url_request_object)    

    
    try:
        response_content = response.read()
        response_json = json.loads(response_content)        
        if not response_json['status']:
            print response_content
            print "request failed"
            print response_json['message']            
            
        return response_json
    finally:
        response.close()        
    return "failed"



#unauthenticated requests
#List of all markets and the status
market_list_request = unauthenticated_request('markets/list/')
variable_test = json.dumps(market_list_request,indent=4)
#print variable_test
#print type(market_list_request)
market_list = market_list_request["markets"]
#print type(market_list)
doge_btc_market=''
for market in market_list:
    if market[u'pair'] == 'DOGE_BTC':
        doge_btc_market=market

print doge_btc_market[u'status']+'\n'+doge_btc_market[u'trade_fee']


    

#List of all coins and the status
#coin_list_request = unauthenticated_request('coins/list/')
#print coin_list_request

#get consolidated market data
#consolidate_market_data_request = unauthenticated_request('markets/data/')
#print consolidate_market_data_request
