import urllib,urllib2
from datetime import datetime
import time
from decimal import *
import hmac
import hashlib
import json

COINS_E_BASE_URL="https://www.coins-e.com/api/v2"
CRYPTSY_BASE_URL="http://pubapi.cryptsy.com/"
CRYPTSY_AUTHENTICATED_BASE_URL="https://www.cryptsy.com/"

COINS_E_MARKET_INFO_SUFFIX="markets/list/"
COINS_E_COINS_PRICE_SUFFIX="markets/data/"
CRYPTSY_MARKET_INFO_SUFFIX="api.php?method=singlemarketdata&marketid=132"

CRYPTSY_PUBLIC_KEY="dfd418a60a3f2538d943a1962cb63c4cf713dce0"
CRYPTSY_PRIVATE_KEY="6a83839dede2e3b807fa2af22a074d8362ec6d09bb1d402fbe0bea9cd46f78995d37385c67319ffc"
COINS_E_PUBLIC_KEY="9d8f25e420e549abe054c187e3463800ba1d88979baef9512546a25a"
COINS_E_PRIVATE_KEY="7390a381a238c5bf58b5cc9a1a55857826c8ce830fcee36b90884419bf897571"

def unauthenticated_request(base_url, url_suffix):
    url_request_object = urllib2.Request("%s/%s" % (base_url,url_suffix))
    response = urllib2.urlopen(url_request_object)  
    response_json = {}
    try:
        response_content = response.read()
        response_json = json.loads(response_content)        
        return response_json
    finally:
        response.close()
    return "failed"

def coins_e_authenticated_request(url_suffix, method, post_args={}):
    nonce = 1000    
    try:
        f = open('coins-e_nonce', 'r')
        nonce = int(f.readline())
        f.close()
    finally:        
        f = open('coins-e_nonce', 'w')
        nonce += 1
        f.write(str(nonce))
        f.close()
    print "coins e nonce=%s" % (nonce)
    post_args['method'] = method        
    post_args['nonce'] = nonce        
    post_data = urllib.urlencode(post_args)
    required_sign = hmac.new(COINS_E_PRIVATE_KEY, post_data, hashlib.sha512).hexdigest()
    headers = {}
    headers['key'] = COINS_E_PUBLIC_KEY
    headers['sign'] = required_sign
    url_request_object = urllib2.Request("%s/%s" % (COINS_E_BASE_URL,url_suffix),
                                         post_data,
                                         headers)    
    response = urllib2.urlopen(url_request_object)    

    
    try:
        response_content = response.read()
        response_json = json.loads(response_content)        
        if not response_json['status']:
            print response_content
            print "request failed"
            print response_json['message']            
            
        return response_json
    finally:
        response.close()        
    return "failed"

def cryptsy_authenticated_request(url_suffix, method, post_args={}):
    nonce = 1000    
    try:
        f = open('cryptsy_nonce', 'r')
        nonce = int(f.readline())
        f.close()
    finally:        
        f = open('cryptsy_nonce', 'w')
        nonce += 1
        f.write(str(nonce))
        f.close()
    print "cryptsy nonce=%s" % (nonce)
    post_args['method'] = method        
    post_args['nonce'] = nonce        
    post_data = urllib.urlencode(post_args)
    required_sign = hmac.new(CRYPTSY_PRIVATE_KEY, post_data, hashlib.sha512).hexdigest()
    headers = {}
    headers['key'] = CRYPTSY_PUBLIC_KEY
    headers['sign'] = required_sign
    url_request_object = urllib2.Request("%s/%s" % (CRYPTSY_AUTHENTICATED_BASE_URL,url_suffix),
                                         post_data,
                                         headers)    
    response = urllib2.urlopen(url_request_object)    

    
    try:
        response_content = response.read()
        response_json = json.loads(response_content)        
        if not response_json['success']:
            print response_content
            print "request failed"
            print response_json['error']            
            
        return response_json
    finally:
        response.close()        
    return "failed"


def cryptsy_price():
    raw = unauthenticated_request(CRYPTSY_BASE_URL, CRYPTSY_MARKET_INFO_SUFFIX)
    doge_info = raw["return"]["markets"]["DOGE"]["lasttradeprice"]
    return doge_info

def coins_e_price():
    raw = unauthenticated_request(COINS_E_BASE_URL, COINS_E_COINS_PRICE_SUFFIX)
    doge_info = raw["markets"]["DOGE_BTC"]["marketstat"]["ltp"]
    return doge_info

def coins_e_wallet_info(coin):
    wallet_info={}
    raw = coins_e_authenticated_request("wallet/%s/" % (coin),"getwallet")
    wallet_info=raw["wallet"]
    return wallet_info
def cryptsy_wallet_info(coin):
    wallet_info={}
    raw = cryptsy_authenticated_request("api","getinfo")
    wallet_info["available"]=raw["return"]["balances_available"][coin]
    wallet_info["onhold"]=raw["return"]["balances_hold"][coin]
    wallet_info["unconfirmed"]=-1
    return wallet_info

cryptsy_price_info = cryptsy_price()
coins_e_price_info = coins_e_price()
print "Cryptsy Price: " + cryptsy_price_info
print "Coins-E Price: " + coins_e_price_info
coins_e_doge_wallet = coins_e_wallet_info("DOGE")
coins_e_btc_wallet = coins_e_wallet_info("BTC");
print "\nCoins-E Available: %s DOGE, %s BTC" % (coins_e_doge_wallet["available"], coins_e_btc_wallet["available"])
print "Coins-E On Hold: %s DOGE, %s BTC" % (coins_e_doge_wallet["onhold"], coins_e_btc_wallet["onhold"])
cryptsy_doge_wallet = cryptsy_wallet_info("DOGE")
cryptsy_btc_wallet = cryptsy_wallet_info("BTC")

print "\nCryptsy Available: %s DOGE, %s BTC" % (cryptsy_doge_wallet["available"], cryptsy_btc_wallet["available"])
print "Cryptsy On Hold: %s DOGE, %s BTC" % (cryptsy_doge_wallet["onhold"], cryptsy_btc_wallet["onhold"])



